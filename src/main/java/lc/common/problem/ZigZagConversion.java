package lc.common.problem;
public class ZigZagConversion {

    public static void main(String[] args) {
        ZigZagConversion zzc = new ZigZagConversion();
        System.out.println(zzc.convert("PAHNAPLSIIGYIR",3));
    }
    public String convert(String s, int numRows) {
        if(numRows==1){
            return s;
        }
        String output="";
        int row=0;
        while(row<numRows){
            int start=0;
            while(start+row<=s.length()-1){
                int next=start+(2*numRows)-2;
                output=output+s.charAt(start+row);
                if(row!=0){
                    if((start+row)!=(next-row)
                            && (next-row)<s.length()){
                        output=output+s.charAt(next-row);
                    }
                }
                start=next;
            }
            row++;
        }
        return output;
    }
}
